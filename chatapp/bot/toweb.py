from transformers import pipeline
from functools import lru_cache

import multiprocessing
import codecs
import os
from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

class Model():

    context = BASE_DIR/"bot/sw_merge.txt"
    

    @lru_cache(maxsize=10000)
    def __init__(self):
        print('processing - init in model')
        print('model location: ', os.path.dirname(os.path.realpath(__file__)) + '/qa_distil2/')
        self.model = pipeline('question-answering', model = os.path.dirname(os.path.realpath(__file__)) + '/qa_distil2/', framework = 'tf')
        with codecs.open(self.context, 'rb', errors = 'ignore', encoding='utf-8') as f:
            self.lines = f.read()

    def run_qa(self, qn):
        print('run_qa - on proccessing')
        ans = self.model(context = self.lines, question = qn)
        print('run_qa done processing')
        return ans


class Conversation():
    #incoming messages - receives an input from the user
    def incoming(self, question):
        usr_qn = []
        usr_qn.append(question)
        return usr_qn

    #model prediction
    def model_ans(self, input_qn):
        y = Model()
        ans = y.run_qa(input_qn)
        ans_text = ans.get("answer")
        print('model_ans - running')
        return ans_text

    def test(self, input):
        return input




# if __name__ == '__main__':

#     p1 = multiprocessing.Process(target=Conversation)
#     p2 = multiprocessing.Process(target=Model)

#     p1.start()
#     p2.start()

#     p1.join()
#     p2.join()


#     # check if processes are alive 
#     print("Process p1 is alive: {}".format(p1.is_alive())) 
#     print("Process p2 is alive: {}".format(p2.is_alive()))

    # question = ''
    # chat_conv = Conversation()

    # incoming_text = chat_conv.incoming(question)
    # outgoing_text = chat_conv.model_ans(incoming_text)
    



